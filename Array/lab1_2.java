import java.util.Arrays;

public class lab1_2 {
    public static void duplicateZeros(int[] arr) {
        int n = arr.length;
        int i = 0;
    
        while (i < n) {
            if (arr[i] == 0) {
                shiftArrayRight(arr, i);
                i += 2; // Skip the duplicated zero and the original zero
            } else {
                i++;
            }
        }
    }
    
    // Helper function to shift the array to the right starting from index i
    private static void shiftArrayRight(int[] arr, int i) {
        int n = arr.length;
        int prev = 0;
    
        for (int j = i; j < n; j++) {
            int temp = arr[j];
            arr[j] = prev;
            prev = temp;
        }
    }
     public static void main(String[] args) {
        int[] arr1 = {1, 0, 2, 3, 0, 4, 5, 0};
        duplicateZeros(arr1);
        System.out.println("Output for Example 1: " + Arrays.toString(arr1));

        int[] arr2 = {1, 2, 3};
        duplicateZeros(arr2);
        System.out.println("Output for Example 2: " + Arrays.toString(arr2));
    }
    
}
